#ifndef DATOS_MEM_COMPARTIDA
#define DATOS_MEM_COMPARTIDA

#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida{
public:
  Raqueta raqueta;
  Esfera esfera;
  int accion; //1 arriba, 0 nada, -1 abajo
};

#endif /* end of include guard: DATOS_MEM_COMPARTIDA */
