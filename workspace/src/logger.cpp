#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdio>

int main(int argc, char const *argv[]) {

  unlink("/tmp/FIFO_tenis");

  if(mkfifo("/tmp/FIFO_tenis", 0777)==-1){
    perror("Error al crear la tubería");
    unlink("/tmp/FIFO_tenis");
    return 1;
  }

  if(fork()==0){
    execlp("./tenis", "./tenis");
    return 1;
  }

  int fd_FIFO;
  int buffer[2];
  fd_FIFO=open("/tmp/FIFO_tenis",O_RDONLY);

  printf(" Jugador 1 | Jugador 2 \n");
  while(read(fd_FIFO,buffer,2*sizeof(int))>1){
    printf("     %d     |     %d     \n", buffer[0], buffer[1]);
  }

  close(fd_FIFO);
  unlink("/tmp/FIFO_tenis");

  return 0;
}
