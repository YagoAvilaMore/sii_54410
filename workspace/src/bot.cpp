#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include <cstdio>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char const *argv[]) {
  int current_ppid=getppid();
  DatosMemCompartida* pdatos;
  int fd=open("/tmp/bot_tenis", O_RDWR, 0777);

	pdatos=(DatosMemCompartida*)mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	close(fd);
  while(getppid()==current_ppid){
    if (pdatos->esfera.centro.y >= pdatos->raqueta.y1-0.5f) {
      pdatos->accion=1;
    } else if (pdatos->esfera.centro.y <= pdatos->raqueta.y2+0.5f) {
      pdatos->accion=-1;
    }
    else{
      pdatos->accion=0;
    }
    usleep(25000);
  }
  return 0;
}
