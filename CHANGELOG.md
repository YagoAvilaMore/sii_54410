# Version 1.3 (Practica 3)

-Se ha creado el programa "logger", que se encarga de mostrar el contador, y ejecutar el juego.
-Se ha creado el programa "bot", que es capaz de controlar al jugador 2. Se ejecuta desde tenis con la tecla 'n' y se le envia la señal SIGTERM con la misma tecla.

# Version 1.2 (Practica 2)

-Se completan las funciones Raqueta:Mueve(float t) y Esfere:Mueve(float t), completando así todos los métodos que faltaban y haciéndolo jugable.
-Se ha creado un archivo README.txt donde se explican los controles.

# Version 1.1 (Practica 1)

-Se ha modificado el archivo Mundo.cpp para agregar la autoría del mismo.
-Se ha añadido el archivo .gitignore para sacar del control de versiones los archivos generados en la carpeta de compilación workspace/build.
-Se ha añadido el archivo CHANGELOG.md para el control de versiones.
